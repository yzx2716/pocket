<?php

namespace Kangaroo\Pocket;

use Kangaroo\Pocket\Exceptions\Exception;
use Kangaroo\Pocket\Requests\Requestable;

class ClientMessage implements ClientInterface
{
    use Requestable;

    public $appId;
    public $appCode;
    public $signSecret;
    public $secretKey;
    public $secretIv;
    public $hosts = [
        'development' => 'https://life.daishujiankang.com/stage-api/wx/openapi/cashier/trade/pushNotify',
        'testing'     => 'https://life.daishujiankang.com/stage-api/wx/openapi/cashier/trade/pushNotify',
        'production'  => 'https://www.fulizhushou.cn/prod-api/wx/openapi/cashier/trade/pushNotify',
    ];
    public $apiUrl;
    public $timestamp;
    //退款
    public $refund = [
        'development' => 'https://life.daishujiankang.com/stage-api/wx/openapi/cashier/trade/refund',
        'testing'     => 'https://life.daishujiankang.com/stage-api/wx/openapi/cashier/trade/refund',
        'production'  => 'https://www.fulizhushou.cn/prod-api/wx/openapi/cashier/trade/refund',
    ];
    //订单状态
    public $orderStatus = [
        'development' => 'https://life.daishujiankang.com/stage-api/wx/openapi/cashier/trade/pushOrderStatus',
        'testing'     => 'https://life.daishujiankang.com/stage-api/wx/openapi/cashier/trade/pushOrderStatus',
        'production'  => 'https://www.fulizhushou.cn/prod-api/wx/openapi/cashier/trade/pushOrderStatus',
    ];
    //开方拿药，给医生下0元订单
    public $sendZeroOrder = [
        'development' => 'https://life.daishujiankang.com/stage-api/wx/openapi/cashier/trade/pay',
        'testing'     => 'https://life.daishujiankang.com/stage-api/wx/openapi/cashier/trade/pay',
        'production'  => 'https://www.fulizhushou.cn/prod-api/wx/openapi/cashier/trade/pay',
    ];
    //查询用户签约有礼标志位接口
    public $getContractGift = [
        'development' => 'https://life.daishujiankang.com/stage-api/app/api/activities/contract/with/courtesy/get/tag',
        'testing'     => 'https://life.daishujiankang.com/stage-api/app/api/activities/contract/with/courtesy/get/tag',
        'production'  => 'https://www.fulizhushou.cn/prod-api/app/api/activities/contract/with/courtesy/get/tag',
    ];
    //签约有礼活动首单修改标志位
    public $consumeContractGift = [
        'development' => 'https://life.daishujiankang.com/stage-api/app/api/activities/contract/with/courtesy/modify/tag',
        'testing'     => 'https://life.daishujiankang.com/stage-api/app/api/activities/contract/with/courtesy/modify/tag',
        'production'  => 'https://www.fulizhushou.cn/prod-api/app/api/activities/contract/with/courtesy/modify/tag',
    ];

    public function __construct($appId = '', $appCode = '', $signSecret = '', $secretKey = '', $secretIv = '', $env = 'production', $mod = 'pushNotify') {
        $this->appId        = $appId;
        $this->appCode      = $appCode;
        $this->signSecret   = $signSecret;
        $this->secretKey    = $secretKey;
        $this->secretIv     = $secretIv;
        switch ($mod) {
            case 'pushNotify':
                $this->apiUrl       = $this->hosts[$env];
                break;
            case 'refund':
                $this->apiUrl       = $this->refund[$env];
                break;
            case 'orderStatus':
                $this->apiUrl       = $this->orderStatus[$env];
                break;
            case 'sendZeroOrder':
                $this->apiUrl       = $this->sendZeroOrder[$env];
                break;
            case 'getContractGift':
                $this->apiUrl       = $this->getContractGift[$env];
                break;
            case 'consumeContractGift':
                $this->apiUrl       = $this->consumeContractGift[$env];
                break;
        }
        $this->timestamp    = round(microtime(true) * 1000);
    }

    /**
     * 请求接口，发送消息
     * @param $params
     * @param string $verb
     * @return mixed|string
     */
    public function sendMessage($contents){
        //拼接参数
        $params = [];
        $params['appId'] = $this->appId;
        $params['appCode'] = $this->appCode;
        $nonce = $this->randomString(6);
        $params['nonce'] = $nonce;
        $params['sign'] = $this->getSign($nonce);
        $params['timestamp'] = $this->timestamp;
        $params['contents'] = $this->getEncryptContent($contents);
        //请求接口
        $result = $this->run($params);
        return $result;
    }

    /**
     * 生成随机字符串
     * @param $len
     */
    public function randomString($len){
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle(str_repeat($pool, ceil($len / strlen($pool)))), 0, $len);
    }

    /**
     * 获取sign签名
     */
    public function getSign($nonce){
        $sign = sha1($this->signSecret . $this->appId . $this->timestamp . $nonce . $this->signSecret);
        return $sign;
    }

    /**
     * 生成加密的内容
     */
    public function getEncryptContent($contents){
        $sign_contents = openssl_encrypt(json_encode($contents), 'AES-128-CBC', $this->secretKey, 0, $this->secretIv);
        $sign_contents = strtolower(bin2hex(base64_decode($sign_contents)));
        return $sign_contents;
    }

    public function run($params = [], $verb = 'POST')
    {
        $head = [];
        $verb = strtolower($verb);
        $head = ['Content-Type:application/json; charset=UTF-8'];
        //fix
        if($verb == 'get' && !empty($params)){
            if(strpos($this->apiUrl, '?') !== false){
                $this->apiUrl .=   '&' . http_build_query($params);
            }else {
                $this->apiUrl .= '?' . http_build_query($params);
            }
        }

        $result = $this->myApi($this->apiUrl, $params, $verb, $head, 10, 'json');

        return $result;
    }
}

