<?php

namespace Kangaroo\Pocket\Requests;

use Kangaroo\Pocket\Exceptions\Exception;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

trait Requestable
{
    function myApi($url,$data = [], $verb = 'get', $header = [],$timeOut = 10, $data_type='')
    {
        $client = new Client();
        try {
            if ($verb == 'get') {
                $response = $client->request(
                    $verb,
                    $url,
                    [
                        'headers' => $header,
                        'timeout' => $timeOut
                    ]
                );
            } elseif('json' == $data_type) {
                $response = $client->request(
                    $verb,
                    $url,
                    [
                        'json' => $data,
                        'headers' => $header,
                        'timeout' => $timeOut
                    ]
                );
            } else {
                $response = $client->request(
                    $verb,
                    $url,
                    [
                        'form_params' => $data,
                        'headers' => $header,
                        'timeout' => $timeOut
                    ]
                );
            }
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return $e->getMessage();
        }
        $jsonBody = $response->getBody()->getContents();
        $arr = json_decode($jsonBody, true);
        if (json_last_error() !== 0) {
            return ($jsonBody);
        }
        return $arr;
    }
}
